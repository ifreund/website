# Isaac Freund's website

This is my personal website hosted at https://isaacfreund.com.

It is built using the [Bagatto](https://bagatto.co) static site generator.

## Licensing

The source code of this website is released under the GNU Affero General Public
License v3.0 only.

The content of this website is released under the CC BY-NC-SA 4.0 License.

See the [LICENSES](LICENSES) directory for full license texts.
