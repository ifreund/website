{:title "About"
 :slug "about"
 :type :markdown}
%%%
Hello, I'm Isaac Freund. I create things, the most public of which are currently
my free and open source software projects.

My largest project is currently the [river](/software/river/) Wayland
compositor.  I also spend significant time on the upstream projects river
depends on, I'm a [wlroots](https://gitlab.freedesktop.org/wlroots/wlroots)
developer and [Zig](https://ziglang.org) core team member.

Outside of my software projects I spend my time playing bass guitar, bouldering,
hiking, reading, and occasionally writing [poetry](/poetry/).

I can be reached by [email](mailto:mail@isaacfreund.com)
([PGP key](/public_key.txt)) and can also be found with the username "ifreund"
on [mastodon](https://hachyderm.io/@ifreund) and various IRC networks.

The source code for my software projects is hosted on
[codeberg](https://codeberg.org/ifreund) with mirrors on
[github](https://github.com/ifreund) and
[sourcehut](https://git.sr.ht/~ifreund).

If my work adds value to your life and you'd like to support me financially you can find donation information [here](/donate/).
