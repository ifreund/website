{:title "Donate"
 :slug "donate"
 :type :markdown}
%%%
The best way to support me financially is with a monthly donation on
[liberapay](https://liberapay.com/ifreund).

You can also support me with a one-time or monthly donation on
[github sponsors](https://github.com/sponsors/ifreund) or
[ko-fi](https://ko-fi.com/ifreund) though I prefer liberapay as it is run by a
non-profit.

Thank you for your support!
