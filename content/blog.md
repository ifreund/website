{:title "Blog"
 :slug "blog"
 :type :markdown}
%%%
My blog consists primarily of updates regarding my free and open source software projects.
Posts are published when I have something significant to share and the time/motivation to write about it.
