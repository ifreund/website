{:title "Consulting"
 :slug "consulting"
 :type :markdown}
%%%
I am a highly experienced software engineer and consultant.

My areas of expertise include:

- Systems programming

- Software architecture

- Protocol design and implementation

- Profiling and optimization

- Compiler and toolchain development

- Open source software

Feel free to reach out by [email](mailto:mail@isaacfreund.com)
for a resume/CV and further information if you are interested in hiring me.
