{:title "Software"
 :slug "software"
 :type :markdown}
%%%
Here's some of the most notable/useful free and open source software I've
written:

- ### [river](/software/river/)
  -- A dynamic tiling Wayland compositor

- ### [waylock](/software/waylock/)
  -- A small and secure Wayland screenlocker

- ### [zig-wayland](https://codeberg.org/ifreund/zig-wayland)
  -- Zig wayland scanner and [libwayland](https://gitlab.freedesktop.org/wayland/wayland) bindings

- ### [zig-wlroots](https://codeberg.org/ifreund/zig-wlroots)
  -- Zig bindings for [wlroots](https://gitlab.freedesktop.org/wlroots/wlroots)
