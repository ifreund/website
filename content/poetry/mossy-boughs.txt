{:title "Mossy Boughs"
 :slug "mossy-boughs"
 :date "2024-03-30"
 :type :poem}
%%%
like green furry fingers
grasping at air
the floor below
thick with dead leaves of the past
and there above
soft green ones emerging

storm
a tree falls
and takes its neighbor with it
seasons later
their children rise to take their place

eons pass
the sun dims and darkens
life slows and ceases
what now?
